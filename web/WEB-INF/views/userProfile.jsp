<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags"  prefix="s"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<sec:authentication var="user2" property="principal" />
<c:if test="${user2.getId()==user.id}">
    it's your page
</c:if>

<h1>hello</h1>

<table>
    <tr>
        <td>Id</td>
        <td>${user.id}</td>
    </tr>
    <tr>
        <td>Name</td>
        <td>${user.nickname}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>${user.email}</td>
    </tr>
    <tr>
        <td>Sex</td>
        <td>
            <c:if test="${user.sex==1}">Мужчина</c:if>
            <c:if test="${user.sex==0}">Женщина</c:if>
        </td>
    </tr>
    <tr>
        <td>Age</td>
        <td>${user.age}</td>
    </tr>
</table>
<a href="/logout">выйти из профиля</a>
<a href="/users">back</a>
</body>
</html>
