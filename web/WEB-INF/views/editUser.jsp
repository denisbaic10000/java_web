<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags"  prefix="s"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create User page</title>
</head>
<body>
<form name="user"  action="/updateUser" method="post">
    <p>Id</p>
    <input title="Id" type="text" name="id" value="${user.id}">
    <p>Name</p>
    <input title="Username" type="text" name="username" value="${user.nickname}">
    <p>Email</p>
    <input title="Email" type="text" name="email" value="${user.email}">
    <p>Password</p>
    <input title="Password" type="text" name="password" value="${user.password}">
    <p>Age</p>
    <input title="Age" type="text" name="age" value="${user.age}">

    <input type="submit" value="ok">
</form>
</body>
</html>