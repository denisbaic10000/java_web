<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags"  prefix="s"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<form name="loginForm" action="/login" method="POST">
    <table>
        <tr>
            <td colspan="2">Login</td>
        </tr>
        <tr>
            <td colspan="2" style="color:red"><c:if test="${not empty error}">Ошибка авторизации, попробуйте снова</c:if> </td>
        </tr>
        <tr>
            <td>Username:</td>
            <td>
                <input type="text" name="nickname"/>
            </td>
        </tr>
        <tr>
            <td>Passowrd:</td>
            <td>
                <input type="password" name="password"/>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" name="Login"/>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
