<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 15.12.2018
  Time: 0:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page import="web.model.CustomUserDetails" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags"  prefix="s"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<sec:authentication var="user2" property="principal"/>
<sec:authentication var="userAuth" property="principal.authorities"/>
<h1>Users list</h1>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Email</th>
        <th>Age</th>
    </tr>
    <c:forEach var="user" items="${users}">
        <tr>
            <td><a href="/user/${user.id}">${user.id}</a></td>
            <td>${user.nickname}</td>
            <td>${user.email}</td>
            <td>${user.age}</td>

            <security:authorize access="hasAnyRole('ADMIN')">
            <td><a href="/delete/${user.id}">Delete</a></td>
            <td><a href="/update/${user.id}">Update</a></td>
            </security:authorize>
        </tr>
    </c:forEach>
</table>
<security:authorize access="hasAnyRole('ADMIN')">
<a href="/addUser">Create user</a>
</security:authorize>
</body>

</html>
</body>
</html>
