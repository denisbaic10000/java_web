package web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.logging.Logger;

@Controller
public class FileController {
    private static final Logger logger = Logger.getLogger(FileController.class.getName());
    @GetMapping(value = "/uploadFile")
    public String getPageUploadFile(){
        return "FileUpload";
    }
    @PostMapping(value = "/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model) {

        String name = null;

        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                name = file.getOriginalFilename();
                String rootPath = "C:\\Users\\Denis\\Downloads\\helloSpring\\src\\main\\webapp\\WEB-INF\\images";
                File dir = new File(rootPath);

                if (!dir.exists()) {
                    dir.mkdirs();
                }
                File uploadedFile = new File(dir.getAbsolutePath() + File.separator + name);

                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(uploadedFile));
                stream.write(bytes);
                stream.flush();
                stream.close();

                logger.info("uploaded: " + uploadedFile.getAbsolutePath());
                model.addAttribute("message","You successfully uploaded file=" + name);
                return "FileUpload";

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "redirect:/uploadFile";
    }

}
