package web.controller;

import org.apache.taglibs.standard.extra.spath.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import web.entity.User;
import web.service.UserService;
import web.service.UserServiceImpl;

@Controller
@RequestMapping("/")
public class LoginController {

@Autowired
public UserService userService;
    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String login(){
        return "login";
    }
    @RequestMapping(value="/login", method = RequestMethod.POST)
    public String loginafter(){
        return "index";
    }
    @GetMapping(value = "/reg")
    public String regPage(){ return "registration";}
    @PostMapping(value="/reg")
    public String saveUser(@ModelAttribute("user") User user, Model model){
        if(userService.findByUsername(user.getNickname())!=null){
            model.addAttribute("nickname_is_used",true);
            return "registration";
        }
        userService.save(user);
        return "redirect:/users";
    }
}