package web.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import web.entity.User;
import web.service.UserService;

public class UserValidator implements Validator {
    @Autowired
    private UserService userService;
    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
    User user=(User) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"username","Required");
        if(user.getNickname().length()<5 || user.getNickname().length()>32){
            errors.rejectValue("username","Size.userForm.username");
        }
        if(userService.findByUsername(user.getNickname())!=null){
            errors.rejectValue("username","Duplicate.userForm.username");
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"password","Required");
        if(user.getPassword().length()<3){
            errors.rejectValue("password","Size.userForm.password");
        }
    }
}
