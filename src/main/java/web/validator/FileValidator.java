package web.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {

        return false;
    }

    @Override
    public void validate(Object uploadedFile, Errors errors) {
        MultipartFile file = (MultipartFile ) uploadedFile;
        String[] Massive=file.getName().replace(" ","").split(".");
        if (file.getSize() == 0) {
            errors.rejectValue("file", "uploadForm.selectFile", "Please select a file!");
        }
        else if(!(Massive[1].equals("jpg")||
                Massive[1].equals("jepg")||
                Massive[1].equals("tiff")||
                Massive[1].equals("tff")||
                Massive[1].equals("png")||
                Massive[1].equals("gif"))){
            errors.reject(null,"It's not image!");
        }
    }
}
