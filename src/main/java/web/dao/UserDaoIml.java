package web.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import web.entity.User;
import web.mapper.RolesMapper;
import web.mapper.UserMapper;

import java.util.List;

public class UserDaoIml implements UserDao {
    @Autowired
    public JdbcTemplate jdbcTemplate;

    public UserDaoIml(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void save(User user) {
        String sql ="insert into alexeydb.user (nickname,email,age,password,sex) values (?,?,?,?,?)";
        jdbcTemplate.update(sql,user.getNickname(),user.getEmail(),user.getAge(),user.getPassword(),user.getSex());
    }

    public User getById(int id) {
        String sql="select * from alexeydb.user where id=?";
        return (User) jdbcTemplate.queryForObject(sql,new UserMapper(),id);
    }
    public void update(User user){
        String sql= "update alexeydb.user set nickname=?,email=?,age=?,password=?, sex=? where id=?";
        jdbcTemplate.update(sql,user.getNickname(),user.getEmail(),user.getAge(),user.getPassword(),user.getSex(),user.getId());
    }
    public void delete(int id) {
        String sql= "delete from alexeydb.user where id=?";
        jdbcTemplate.update(sql,id);
    }

    public List<User> findAll() {
        String sql="select * from alexeydb.user";
        return jdbcTemplate.query(sql,new UserMapper());
    }

    @Override
    public User findByUsername(String username) {
        String sql="select * from alexeydb.user where nickname=?";

        return jdbcTemplate.query(sql,new UserMapper(),username).get(0);
    }

    @Override
    public List<String> getUserRoles(String username) {
        String sql="Select authority from alexeydb.roles where nickname=?";
        List<String> roles= jdbcTemplate.query(sql, new RolesMapper(),username);
        return roles;
    }

}
