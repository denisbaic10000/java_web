package web.dao;

import web.entity.User;

import java.util.List;

public interface UserDao {
    void save(User user);
    User getById(int id);
    public void update(User user);
    void delete(int id);
    List<User> findAll();
    User findByUsername(String username);
    List<String> getUserRoles(String username);
}
