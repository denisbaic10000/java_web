package web.entity;

import java.util.Objects;

public class User {
    private Integer id,age,sex;
    private String nickname, email,password;

    public User() {
    }

    public User(Integer id, String nickname, String password) {
        this.id = id;
        this.nickname = nickname;
        this.password = password;
    }

    public User(Integer id, Integer age, Integer sex, String nickname, String email, String password) {
        this.id = id;
        this.age = age;
        this.sex = sex;
        this.nickname = nickname;
        this.email = email;
        this.password = password;
    }

    public User(User user) {
        this.id=user.getId();
        this.age=user.getAge();
        this.sex=user.getSex();
        this.nickname=user.getNickname();
        this.email=user.getEmail();
        this.password=user.getPassword();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getId(), user.getId()) &&
                Objects.equals(getAge(), user.getAge()) &&
                Objects.equals(getSex(), user.getSex()) &&
                Objects.equals(getNickname(), user.getNickname()) &&
                Objects.equals(getEmail(), user.getEmail()) &&
                Objects.equals(getPassword(), user.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAge(), getSex(), getNickname(), getEmail(), getPassword());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", age=" + age +
                ", sex=" + sex +
                ", nickname='" + nickname + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}

