package web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import web.dao.UserDao;
import web.entity.User;
import web.model.CustomUserDetails;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserDao loginDao;

    public void setLoginDao(UserDao loginDao) {
        this.loginDao = loginDao;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        User user =loginDao.findByUsername(username);
        Set<GrantedAuthority> grantedAuthorities=new HashSet<>();
        for(String role:loginDao.getUserRoles(username)){
        grantedAuthorities.add(new SimpleGrantedAuthority(role));
        };
        return new org.springframework.security.core.userdetails.User(user.getNickname(),user.getPassword(),grantedAuthorities);
    }


//    @Override
//    public CustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//
//        web.entity.User userInfo = loginDao.findByUsername(username);
//
//        if(userInfo==null){
//            throw new UsernameNotFoundException("user name not found");
//        }
//
//        List<String> roles = loginDao.getUserRoles(username);
//
//        List<GrantedAuthority> grantedAuthorityList = new ArrayList<GrantedAuthority>();
//
//        if(roles!=null){
//            for(String role : roles){
//                GrantedAuthority authority = new SimpleGrantedAuthority(role);
//                grantedAuthorityList.add(authority);
//            }
//        }
//        CustomUserDetails customUserDetails=new CustomUserDetails(userInfo,loginDao);
//
//        return customUserDetails;
//    }
}

