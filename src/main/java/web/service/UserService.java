package web.service;

import web.entity.User;

import java.util.List;

public interface UserService {
    List<User> findAll();
    void save(User user);
    User getById(int id);
    public void update(User user);
    void delete(int id);
    public User findByUsername(String username);
}
