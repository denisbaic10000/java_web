package web.mapper;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RolesMapper implements RowMapper<String> {
    public String mapRow(ResultSet resultSet, int i) throws SQLException {
        String role = resultSet.getString("authority");

        return role;
    }
}
