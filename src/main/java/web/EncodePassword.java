package web;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class EncodePassword {
    public static void main(String[] args){
        PasswordEncoder passwordEncoder=  new PasswordEncoder() {
            @Override
            public String encode(CharSequence charSequence) {
                return charSequence.toString();
            }

            @Override
            public boolean matches(CharSequence charSequence, String s) {
                return true;
            }
        };

        String password="125433";
        System.out.println(passwordEncoder.encode(password));
        //BCryptPasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
        //String hashedPassword=passwordEncoder.encode(password);
        //System.out.println(hashedPassword);
    }
}
